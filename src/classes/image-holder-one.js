import config from '../kernel/config';
 
export default class ImageHolderOne {
    constructor (scene) {

        this.scene = scene;
        const xx = scene.game.config.width;
        const yy = scene.game.config.height;
       
        const _img = config.images.auto.image;
        this.spriteHolder = scene.add.image(xx*0.5, yy*0.3, 'imagesAtlas', _img);
        this.spriteHolder.setOrigin(0.5);
        // this.spriteHolder.setScale(0.85);
        this.spriteHolder.setScale(0);
      
    }

    rightGuess(num){
        this.scene.tweens.add({
            targets : this.spriteHolder,
            scale : 0.85 * 1.2,
            ease : 'Linear',
            duration : 100
        });
        this.scene.tweens.add({
            targets : this.spriteHolder,
            scale : 0,
            ease : 'Linear',
            duration : 300,
            delay : 100,
            onComplete : this.showImage(num)            
        });
    }

    showImage(num){
        
        const randNum = Math.round(Math.random());
        const lettersObj = config.letters[`letter${num}`].image;
        const rndomName = lettersObj[randNum];
        const img = config.images[rndomName].image;

        this.spriteHolder.setTexture("imagesAtlas", img);      
        
        this.scene.tweens.add({
            targets : this.spriteHolder,
            scale : 0.85,
            ease : 'Linear',
            duration : 500,
            delay : 100
        });

    }
  
}