
 
export default class Letter {
  constructor (scene, key, id, letterCase, letterSignal) {
    this.scene = scene;
    this.letterSignal = letterSignal;
    this.letterContainer = scene.add.container(0,0);
    const background = scene.add.image(0, 0, 'assets', 'field.png');
    background.setOrigin(0.5);
    const sprite = scene.add.image(0, 10, 'assets', key);
    sprite.setOrigin(0.5);
    sprite.setScale(0.6);
    this.letterContainer.add(background);
    this.letterContainer.add(sprite);
    this.letterContainer.tap = false;
    this.letterId = id;
    this.fullId = `${id}${letterCase}`
    this.letterCase = letterCase;
    this.letterContainer.setSize(background.width, background.height);
    this.letterContainer.setInteractive();

    this.letterContainer.on('pointerdown',()=>{
      this.oneSelected();
      this.letterSignal.emit('letterSignal', this.letterId, this.fullId);
    })

    const xx = scene.game.config.width;
    const yy = scene.game.config.height; 

  }

  oneSelected(){
    this.scene.tweens.add({
      targets : this.letterContainer,
      scale : 0.9,
      ease : 'Linear',
      duration : 100,
      delay : 0,
      onComplete: ()=>{
        this.letterContainer.disableInteractive()
      }
    });
  }

  delectedLetter(sc){
    this.scene.tweens.add({
      targets : this.letterContainer,
      scale : sc,
      ease : 'Linear',
      duration : 100,
      delay : 200,
      onComplete: ()=>{
        this.letterContainer.setInteractive()
      }
    });
  }

}