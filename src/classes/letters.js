import Letter from './letter';
 
export default class Letters {
    constructor (scene) {
        this.scene = scene;
        this.letterSignal = new Phaser.Events.EventEmitter();
        const xx = scene.game.config.width;
        const yy = scene.game.config.height;        
        this.lowerCase = [];
        this.upperCase = [];
        this._createLetters();        
        
        this.upperCaseWidth = this.upperCase[0].letterContainer.width;
        this.upperCaseHeight = this.upperCase[0].letterContainer.height;
        this._resetScale(this.lowerCase);
        this._resetScale(this.upperCase);
        
    }
    
    _createLetters(){
        for(let i = 1; i<31; i++){
            let lowerKey = `${i}M.png`;
            let upperKey = `${i}V.png`;
            let lower = new Letter(this.scene, lowerKey, i, "M", this.letterSignal);
            let upper = new Letter(this.scene, upperKey, i, "V", this.letterSignal);          
            this.lowerCase.push(lower);
            this.upperCase.push(upper);
        }
        
    }

    _resetScale(array){
        array.forEach(element => {
            element.letterContainer.setScale(0)
        });
    }
  
}