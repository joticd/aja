 
export default class FieldLetter {
    constructor (scene) {

        this.scene = scene;
        const xx = scene.game.config.width;
        const yy = scene.game.config.height;

        this.spriteContainer = scene.add.container(0,0);
        this.spriteHolder = scene.add.image(0, 0, 'assets', 'field.png');
        this.spriteHolder.setOrigin(0.5);
        this.spriteHolder.setScale(0.85);
        this.spriteContainer.add(this.spriteHolder);
        for(let i = 1; i < 31; i++){
            const letter = scene.add.image(0, 20, 'assets', `${i}V.png`);
            letter.setOrigin(0.5);
            letter.setScale(0.6 * 0);
            this.spriteContainer.add(letter);
        }
    
        this.spriteContainer.setSize(this.spriteHolder.width, this.spriteHolder.height);
        this.spriteContainer.setInteractive();

        this.spriteContainer.x = xx*0.5;
        this.spriteContainer.y = yy*0.5;
        this.spriteContainer.setScale(0);

      
      
    }

  
}