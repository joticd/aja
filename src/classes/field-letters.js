import FieldLetter from './field-letter';
export default class FieldLetters {
    constructor (scene) {

        this.field = [];
        for(let i = 0; i < 15; i++){
            const _field = new FieldLetter (scene);
            this.field.push(_field);
        }
    }

  
}