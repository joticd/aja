import config from '../../kernel/config';

let scaleLetter = 0.75;

export default class Controller {
  constructor (scene, {upperCase, upperCaseWidth, upperCaseHeight}) {
    // super(scene);
    this.scene = scene;
    this.gameWidth = scene.game.config.width;
    this.gameHeight = scene.game.config.height;

    this.upperCase = upperCase;
    this.upperCaseWidth = upperCaseWidth;
    this.upperCaseHeight = upperCaseHeight;
    this._setInteractiveArr(this.upperCase, false);
    
    this.firstRow = [];
    this.secondRow = [];
    this.thirdRow = [];
    
  }

  prepareLetters(arr){
    this.firstRow = [];
    this.secondRow = [];
    this.thirdRow = [];

    this._lettersLvl0(arr);
    
    this._xCoordLetter0();
    this._showRow(this.firstRow, 0);
    this._showRow(this.secondRow, 300);
    this._showRow(this.thirdRow, 600, true);
    
  }

  _lettersLvl0(array){
   
    let upperCaseCopy = [...this.upperCase];
    array.forEach((element, index) => {        
      if(index<5){
        this.firstRow.push(upperCaseCopy[element-1]);
      }else if(index>4 && index<10){          
        this.secondRow.push(upperCaseCopy[element-1]);          
      }else{          
        this.thirdRow.push(upperCaseCopy[element-1]);
      }
    });
  }

  _xCoordLetter0 (){
    let x1 = this.gameWidth * 0.5 - this.upperCaseWidth*2*scaleLetter - this.upperCaseWidth*2*0.25*scaleLetter;

    this.firstRow.forEach((element, index) => {
      element.letterContainer.x = x1 + this.upperCaseWidth*index*scaleLetter + this.upperCaseWidth*index*0.25*scaleLetter;
      element.letterContainer.y = this.gameHeight*0.5;
    });
    this.secondRow.forEach((element, index) => {
      element.letterContainer.x = this.firstRow[index].letterContainer.x;
      element.letterContainer.y = this.firstRow[index].letterContainer.y + this.upperCaseWidth*scaleLetter + this.upperCaseWidth*0.25*scaleLetter;
    });
    this.thirdRow.forEach((element, index) => {      
      element.letterContainer.x = this.firstRow[index].letterContainer.x;
      element.letterContainer.y = this.secondRow[index].letterContainer.y + this.upperCaseWidth*scaleLetter + this.upperCaseWidth*0.25*scaleLetter;
    });
  }

  _showRow(array, d, bool=false){
    array.forEach((element, index) => {
      this.scene.tweens.add({
        targets : element.letterContainer ,
        scale : 1*scaleLetter,
        ease : 'Linear',
        duration : 500,
        delay : d + index*300,
        onComplete: () => {
          if(bool && index === this.thirdRow.length-1){          
            this._setInteractiveArr(this.firstRow, true);
            this._setInteractiveArr(this.secondRow, true);
            this._setInteractiveArr(this.thirdRow, true);            
          }
        },
      });
    });
  }

  _setInteractiveArr(arr, bool){
    arr.forEach((element, index) => {
      if(bool){
        element.letterContainer.setInteractive();
      } else {
        element.letterContainer.disableInteractive();
      }
    });
  }

  falseGuess(id){
    const _in = this.upperCase.findIndex(element=> element.letterId === id);
    this.upperCase[_in].delectedLetter(1*scaleLetter);
  }

  winGuess(id, arrLetters){
    this.falseGuess(id);
    this._setInteractiveArr(this.firstRow, false);
    this._setInteractiveArr(this.secondRow, false);
    this._setInteractiveArr(this.thirdRow, false);
    this._fadeLetters(this.thirdRow, 0);
    this._fadeLetters(this.secondRow, 300); 
    this._fadeLetters(this.firstRow, 600, true, arrLetters);   
    
  }

  _fadeLetters(arr, d, bool=false, arrLetters=null){
    for(let i = arr.length-1; i > -1; i--){
      this.scene.tweens.add({
        targets : arr[i].letterContainer,
        alpha : 0,
        ease : 'Linear',
        duration : 500,
        delay : d + (arr.length-1-i)*300,
        onComplete:  ()=>{
          if(bool && i === 0){
            this.upperCase.forEach(element => {
              element.letterContainer.alpha=1;
              element.letterContainer.setScale(0);
            });
            this.prepareLetters(arrLetters);
           
          }
        }
      });
    }

  }



   
}