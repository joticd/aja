import config from '../../kernel/config';

export default class Model {
    constructor (scene) {
        
        this.lvl = 0;
        this.letterNum = Math.floor(Math.random()*30)+1;
        this.lettersToFind = [this.letterNum];
        
        this.letterOfferdSignal = new Phaser.Events.EventEmitter();
        this.changeLevel(false);
     
    }

    _createOfferd(){
        console.log(this.letterNum)
        this.lettersOfferd = [];
        this.lettersOfferd.push(this.letterNum);
        this._recOffer();
    }
    
    _recOffer(){
        const _num = Math.floor(Math.random()*30)+1;
        const _boolNum = this.lettersOfferd.some(element=>element===_num);
        if(!_boolNum){
            this.lettersOfferd.push(_num);
        }
        if(this.lettersOfferd.length === this.numberOfLetters){       
            return;
        }
        this._recOffer();
    }
    
    
    changeLevel(bool){
        this.lvl = bool && this.lvl === 0 ? 1 : 0;
        this.numberOfLetters = config.gameOne.numberOfLetters[this.lvl];
        this._createOfferd();        
    }

    guessNumber(id){
        const guess = id === this.lettersToFind[this.lettersToFind.length-1];
        let newLetter = -1;
        let offerd = [];
        if(guess){
            newLetter = this._newLettersToFind();            
            this._createOfferd();
            offerd = this.lettersOfferd;
            this.letterNum = newLetter;
           
        }
        return {guess, newLetter, offerd};
    }


    _newLettersToFind(){
        if(this.lettersToFind.length === 30){
          this._newArrNumber();
        } else {
            this._newNextNumber();
        }
        let _newLetter = this.lettersToFind[this.lettersToFind.length-1];
        this.letterNum = _newLetter;
        return _newLetter;        
    }

    _newNextNumber(){
        let _num = Math.floor(Math.random()*30)+1;        
        let _boolNum = this.lettersToFind.some(element=>element===_num);
        if(_boolNum){
            this._newNextNumber();
        }
        if(!_boolNum){
            this.lettersToFind.push(_num);
            return;
        }       
    }
   

    _newArrNumber(){
        let _num = Math.floor(Math.random()*30)+1;        
        let _boolNum = this.lettersToFind[this.lettersToFind.length-1]===_num;
        if(_boolNum){
            this._newArrNumber();
        }
        if(!_boolNum){
            this.lettersToFind = [_num];
            return;
        }
    }



  
    


}