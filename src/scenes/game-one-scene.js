import 'phaser';
import ImageHolderOne from '../classes/image-holder-one';
import Letters from '../classes/letters';
import Controller from '../functions/one/game-one-controller';
import Model from '../functions/one/game-one-model';
 
export default class GameOneScene extends Phaser.Scene {
  constructor () {
    super('GameOne');
  }
 
  preload () {}
 
  create () {
    const xx = this.game.config.width;
    const yy = this.game.config.height;    

    this.background = this.add.image(xx*0.5, yy*0.5, 'background');
    const txt = this.add.bitmapText(xx*0.5, yy*0.1, "font", "Прва игра", 60);    
    txt.setOrigin(0.5);   
    
    this.letters = new Letters(this);
    this._createBttn(xx*0.1, yy*0.1);
    this._createLvlBttn(xx*0.75, yy*0.1);    
    this._createBackBttn(xx*0.9, yy*0.1);
    
    this.imageHolder = new ImageHolderOne(this);

    this.model = new Model(this);
    this.controller = new Controller(this, this.letters); 

    this._listeners();
  }

  _listeners(){
    const {model, controller, letters, imageHolder} = this;
    const _i = model.lettersToFind.length-1;
    imageHolder.showImage(model.lettersToFind[_i]);    
    controller.prepareLetters(model.lettersOfferd);

    // model.letterOfferdSignal.on('offerdSignal', ()=>{
    //   console.log("222222222222")
    // })

    letters.letterSignal.on('letterSignal', (letterId, fullId)=>{
      let {guess, newLetter, offerd} = model.guessNumber(letterId);      
      if(guess){
        imageHolder.rightGuess(newLetter);
        controller.winGuess(letterId, offerd);
      }else{
        controller.falseGuess(letterId);
      }
    })
  }

  
  _createBttn(x, y){
    const button = this.add.image(x, y, 'assets', 'button.png');
    const txt = this.add.bitmapText(x, y, "font", "Менјанје слике", 30);    
    txt.setOrigin(0.5);
    button.setOrigin(0.5);
    button.setScale(1.5)
    button.setInteractive();
    button.on('pointerdown', ()=>{
      
    });
  }

  _createLvlBttn(x, y){
    const button = this.add.image(x, y, 'assets', 'button.png');
    const txt = this.add.bitmapText(x, y, "font", "Тежина", 30);    
    txt.setOrigin(0.5);
    button.setOrigin(0.5);
    button.setScale(1.5)
    button.setInteractive();
    button.on('pointerdown', ()=>{
      
    });
  }

  _createBackBttn(x, y, num){
    const button = this.add.image(x, y, 'assets', 'button.png');    
    const txt = this.add.bitmapText(x, y, "font", "Главни мени", 30);    
    txt.setOrigin(0.5);
    button.setOrigin(0.5);
    button.setScale(1.5)
    button.setInteractive();
    button.on('pointerdown', ()=>{
      this.scene.start('Game');
    });
  }

  
  
}