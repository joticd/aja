import 'phaser';
 
export default class PreloaderScene extends Phaser.Scene {
  constructor () {
    super('Preloader');
  }
 
  preload () {
    this.add.image(400, 200, 'logo');
    this.timedEvent = this.time.delayedCall(1000, this.ready, [], this);

     // load assets needed in our game
     this.load.image('background', '../../src/assets/background.png');
     this.load.atlas('assets', '../../src/assets/assets.png', '../../src/assets/assets.json');
     this.load.atlas('imagesAtlas', '../../src/assets/images.png', '../../src/assets/images.json');
     this.load.bitmapFont("font", "../src/assets/font.png", "../src/assets/font.fnt");
  }
 
  create () {
  }

  init () {
    this.readyCount = 0;
  }
  
  ready () {    
    this.readyCount++;
    if (this.readyCount === 1) {
      // this.scene.start('Game');
      // this.scene.start('GameOne');
      this.scene.start('GameTwo');
    }
  }
};