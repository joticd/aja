import 'phaser';
 
export default class GameScene extends Phaser.Scene {
  constructor () {
    super('Game');
  }
 
  preload () {}
 
  create () {
    const xx = this.game.config.width;
    const yy = this.game.config.height;
    this.background = this.add.image(xx*0.5, yy*0.5, 'background');
    
    const button1 = this.add.image(xx*0.5, yy*0.5, 'assets', 'button.png');
    
    button1.setScale(2);
    button1.setInteractive();
    button1.on('pointerdown', ()=>{
      this.scene.start('GameOne');
    });


  }
};