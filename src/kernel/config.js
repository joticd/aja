//А, Б, В, Г, Д, Ђ, Е, Ж, З, И, Ј, К, Л, Љ, М, Н, Њ, О, П, Р, С, Т, Ћ, У, Ф, Х, Ц, Ч, Џ, Ш, а, б, в, г, д, ђ, е, ж, з, и, ј, к, л, љ, м, н, њ, о, п, р, с, т, ћ, у, ф, х, ц, ч, џ, ш

export default {
    letters: {
        letter1 : {
            letter : "1V.png",
            image : ["auto", "avion"]
        },
        letter2 : {
            letter : "2V.png",
            image : ["balon", "bubamara"]
        },
        letter3 : {
            letter : "3V.png",
            image : ["vatra", "vulkan"]
        },
        letter4 : {
            letter : "4V.png",
            image : ["golub", "guma"]
        },        
        letter5 : {
            letter : "5V.png",
            image : ["drvo", "dugme"]
        },
        letter6 : {
            letter : "6V.png",
            image : ["djak", "djubre"]
        },
        letter7 : {
            letter : "7V.png",
            image : ["ekran", "ekser"]
        },
        letter8 : {
            letter : "8V.png",
            image : ["zaba", "zbun"]
        },
        letter9 : {
            letter : "9V.png",
            image : ["zmaj", "zub"]
        },
        letter10 : {
            letter : "10V.png",
            image : ["igla", "iglo"]
        },

        
        letter11 : {
            letter : "11V.png",
            image : ["jabuka", "jagoda"]
        },
        letter12 : {
            letter : "12V.png",
            image : ["kifla", "kukuruz"]
        },
        letter13 : {
            letter : "13V.png",
            image : ["leptir", "lizalica"]
        },
        letter14 : {
            letter : "14V.png",
            image : ["ljubav", "ljuljaska"]
        },
        letter15 : {
            letter : "15V.png",
            image : ["macka", "mis"]
        },
        letter16 : {
            letter : "16V.png",
            image : ["naocare", "novcanik"]
        },
        letter17 : {
            letter : "17V.png",
            image : ["njiva", "njuska"]
        },
        letter18 : {
            letter : "18V.png",
            image : ["oko", "ostrvo"]
        },
        letter19 : {
            letter : "19V.png",
            image : ["palacinke", "prsten"]
        },
        letter20 : {
            letter : "20V.png",
            image : ["ranac", "rep"]
        },
        letter21 : {
            letter : "21V.png",
            image : ["sladoled", "sunce"]
        },
        letter22 : {
            letter : "22V.png",
            image : ["tasna", "truba"]
        },
        letter23 : {
            letter : "23V.png",
            image : ["cilim", "cufte"]
        },
        letter24 : {
            letter : "24V.png",
            image : ["ukras", "uvo"]
        },
        letter25 : {
            letter : "25V.png",
            image : ["fioka", "flauta"]
        },
        letter26 : {
            letter : "26V.png",
            image : ["hleb", "hrana"]
        },
        letter27 : {
            letter : "27V.png",
            image : ["cipela", "cucla"]
        },
        letter28 : {
            letter : "28V.png",
            image : ["camac", "cigra"]
        },
        letter29 : {
            letter : "29V.png",
            image : ["dzak", "dzem"]
        },
        letter30 : {
            letter : "30V.png",
            image : ["sisarka", "suma"]
        }
    },
    images: {
        auto: {
            image: "auto.png",
            word: [1, 24, 22, 18]
        },
        avion: {
            image: "avion.png",
            word: [1, 3, 10, 18, 16]
        },
        balon: {
            image: "balon.png",
            word: [2, 1, 13, 18, 16]
        },
        bubamara: {
            image: "bubamara.png",
            word: [2, 24, 2, 1, 15, 1, 20, 1]
        },
        camac: {
            image: "camac.png",
            word: [28, 1, 15, 1, 27]
        },
        cigra: {
            image: "cigra.png",
            word: [28, 10, 4, 20, 1]
        },
        cilim: {
            image: "cilim.png",
            word: [23, 10, 13, 10, 15]
        },
        cipela: {
            image: "cipela.png",
            word: [27, 10, 19, 7, 13, 1]
        },
        cucla: {
            image: "cucla.png",
            word: [27, 24, 27, 13, 1]
        },
        cufte: {
            image: "cufte.png",
            word: [23, 24, 25, 22, 7]
        },
        djak: {
            image: "djak.png",
            word: [6, 1, 12]
        },
        djubre: {
            image: "djubre.png",
            word: [6, 24, 2, 20, 7]
        },
        drvo: {
            image: "drvo.png",
            word: [5, 20, 3, 18]
        },
        dugme: {
            image: "dugme.png",
            word: [5, 24, 4, 15, 7]
        },
        dzak: {
            image: "dzak.png",
            word: [29, 1, 12]
        },
        dzem: {
            image: "dzem.png",
            word: [29, 7, 15]
        },
        ekran: {
            image: "ekran.png",
            word: [7, 12, 20, 1, 16]
        },
        ekser: {
            image: "ekser.png",
            word: [7, 12, 21, 7, 20]
        },
        fioka: {
            image: "fioka.png",
            word: [25, 10, 18, 12, 1]
        },
        flauta: {
            image: "flauta.png",
            word: [25, 13, 1, 24, 22, 1]
        },
        golub: {
            image: "golub.png",
            word: [4, 18, 13, 24, 2]
        },
        guma: {
            image: "guma.png",
            word: [4, 24, 15, 1]
        },
        hleb: {
            image: "hleb.png",
            word: [26, 13, 7, 2]
        },
        hrana: {
            image: "hrana.png",
            word: [26, 20, 1, 16, 1]
        },
        igla: {
            image: "igla.png",
            word: [10, 4, 13, 1]
        },
        iglo: {
            image: "iglo.png",
            word: [10, 4, 13, 18]
        },
        jabuka: {
            image: "jabuka.png",
            word: [11, 1, 2, 24, 12, 1]
        },
        jagoda: {
            image: "jagoda.png",
            word: [11, 1, 4, 18, 5, 1]
        },
        kifla: {
            image: "kifla.png",
            word: [12, 10, 25, 13, 1]
        },
        kukuruz: {
            image: "kukuruz.png",
            word: [12, 24, 12, 24, 20, 24, 9]
        },
        leptir: {
            image: "leptir.png",
            word: [13, 7, 19, 22, 10, 20]
        },
        lizalica: {
            image: "lizalica.png",
            word: [13, 10, 9, 1, 13, 10, 27, 1]
        },
        ljubav: {
            image: "ljubav.png",
            word: [14, 24, 2, 1, 3]
        },
        ljuljaska: {
            image: "ljuljaska.png",
            word: [14, 24, 14, 1, 30, 12, 1]
        },
        macka: {
            image: "macka.png",
            word: [15, 1, 27, 12, 1]
        },
        mis: {
            image: "mis.png",
            word: [15, 10, 30]
        },
        naocare: {
            image: "naocare.png",
            word: [16, 1, 18, 28, 1, 20, 7]
        },
        njiva: {
            image: "njiva.png",
            word: [17, 10, 3, 1]
        },
        njuska: {
            image: "njuska.png",
            word: [17, 24, 30, 12, 1]
        },
        novcanik: {
            image: "novcanik.png",
            word: [16, 18, 3, 28, 1, 16, 10, 12]
        },
        oko: {
            image: "oko.png",
            word: [18, 12, 18]
        },
        ostrvo: {
            image: "ostrvo.png",
            word: [18, 21, 22, 20, 3, 18]
        },
        palacinke: {
            image: "palacinke.png",
            word: [19, 1, 13, 1, 28, 10, 16, 12, 7]
        },
        prsten: {
            image: "prsten.png",
            word: [19, 20, 21, 22, 7, 16]
        },
        ranac: {
            image: "ranac.png",
            word: [20, 1, 16, 1, 27]
        },
        rep: {
            image: "rep.png",
            word: [20, 7, 19]
        },
        sisarka: {
            image: "sisarka.png",
            word: [30, 10, 30, 1, 20, 12, 1]
        },
        sladoled: {
            image: "sladoled.png",
            word: [21, 13, 1, 5, 18, 13, 7, 5]
        },
        suma: {
            image: "suma.png",
            word: [30, 24, 15, 1]
        },
        sunce: {
            image: "sunce.png",
            word: [21, 24, 16, 27, 7]
        },
        tasna: {
            image: "tasna.png",
            word: [22, 1, 30, 16, 1]
        },
        truba: {
            image: "truba.png",
            word: [22, 20, 24, 2, 1]
        },
        ukras: {
            image: "ukras.png",
            word: [24, 12, 20, 1, 21]
        },
        uvo: {
            image: "uvo.png",
            word: [24, 3, 18]
        },
        vatra: {
            image: "vatra.png",
            word: [3, 1, 22, 20, 1]
        },
        vulkan: {
            image: "vulkan.png",
            word: [3, 24, 13, 12, 1, 16]
        },
        zaba: {
            image: "zaba.png",
            word: [8, 1, 2, 1]
        },
        zbun: {
            image: "zbun.png",
            word: [8, 2, 24, 16]
        },
        zmaj: {
            image: "zmaj.png",
            word: [9, 15, 1, 11]
        },
        zub: {
            image: "zub.png",
            word: [9, 24, 2]
        }
    },
    gameOne : {
        numberOfLetters : [15, 30]
    }
}